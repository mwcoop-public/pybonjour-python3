from distutils.core import setup


setup(
    name = 'pybonjour',
    version = '1.1.1',
    author = 'Christopher Stawarz',
    author_email = 'cstawarz@csail.mit.edu',
    url = 'http://o2s.csail.mit.edu/o2s-wiki/pybonjour',
    description = 'Pure python interface to Apple Bonjour and compatible libraries.',
    download_url = '',
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Topic :: System :: Distributed Computing',
        'Topic :: System :: Networking',
        ],
    py_modules = ['pybonjour'],
    )
